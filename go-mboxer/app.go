package gomboxer

import (
	"context"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
)

var (
	X           *xgb.Conn
	err         error
	Windows     []Window
	LastFocused int
)

func NewApp() *App {
	return &App{}
}

func (a *App) Startup(ctx context.Context) {
	a.ctx = ctx

	f, err := os.OpenFile("/tmp/go-mboxer.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Printf("Error opening file: %v", err)
	}
	defer f.Close()

	log.SetOutput(f)

	log.Println("Logging initialized")

	X, err = xgb.NewConn()
	if err != nil {
		log.Printf("Error connecting to X: %v", err)
	}

	xproto.Setup(X).DefaultScreen(X)

	LastFocused = 0
	NewHotkeys().Startup(ctx)
}

func (a *App) beforeClose(ctx context.Context) (prevent bool) {
	X.Close()
	return false
}

func (a *App) OnShutdown(ctx context.Context) {
	X.Close()
}

func (a *App) GetClients() (matchingWindows []Window) {
	root := xproto.Setup(X).DefaultScreen(X).Root

	tree, err := xproto.QueryTree(X, root).Reply()
	if err != nil {
		log.Printf("Error querying tree: %v", err)
	}

	re, err := regexp.Compile(`EVE - [a-zA-Z0-9]`)
	if err != nil {
		log.Printf("Error compiling Regex: %v",err)
	}

	for _, win := range tree.Children {
		winID := win
		winName := getWindowName(winID)

		if re.MatchString(winName) {
			winName = trimSubstr(winName, "EVE - ")
			matchingWindows = append(matchingWindows, Window{ID: int64(winID), Name: winName})
		}

		sort.Slice(matchingWindows, func(i, j int) bool {
			return matchingWindows[i].ID < matchingWindows[j].ID
		})
	}

	// MonitorWindows(X, re, matchingWindows)
	Windows = matchingWindows

	return
}

func trimSubstr(s string, substr string) (t string) {
	for {
		t = strings.TrimPrefix(s, substr)
		t = strings.TrimSuffix(t, substr)
		if t == s {
			break
		}
		s = t
	}

	return
}

func getWindowName(window xproto.Window) (winName string) {
	win, err := xproto.GetProperty(X, false, xproto.Window(window), xproto.AtomWmName, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
	if err != nil {
		log.Printf("Error fetching windowName: %v", err)
	}

	winName = string(win.Value)

	return
}

func (a *App) GetFocusedWindow() (uint32, error) {
	focus, err := xproto.GetInputFocus(X).Reply()
	if err != nil {
		log.Printf("Error getting focused window: %v", err)
		return 0, err
	}

	return uint32(focus.Focus), nil
}

func (a *App) FocusWindow(windowID int64) (bool, error) {
	if err != nil {
		log.Printf("Error activating window: %v", err)
		return false, err
	}

	xproto.SetInputFocus(X, xproto.InputFocusPointerRoot, xproto.Window(windowID), xproto.TimeCurrentTime)
	return true, nil
}

func (a *App) ElectNewLeader(newLeader int) bool {
	log.Printf("Received new leader elect: %v", Windows[newLeader].Name)
	Leader = newLeader
	return true
}
