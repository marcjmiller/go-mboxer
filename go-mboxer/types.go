package gomboxer

import (
	"context"
)

type App struct {
	ctx context.Context
}

type Hotkeys struct {
	ctx context.Context
}

type Window struct {
	ID   int64
	Name string
}

type Game struct {
	Name         string
	Regex        string
	WindowPrefix string
	WindowSuffix string
}

type Games struct {
	GameList []Game
}
