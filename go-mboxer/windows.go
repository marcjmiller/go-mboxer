package gomboxer

import (
	"log"
	"regexp"
	"strings"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil/xevent"
)

func MonitorWindows(X *xgb.Conn, re *regexp.Regexp, matchingWindows []Window) {
	go func() {
		log.Println("Start monitoring windows")
		for {
			event, err := X.WaitForEvent()
			if err != nil {
				log.Println("Error with x windows connection")
			}

			log.Printf("Caught Window Event: %v \n", event)

			switch event.(type) {
			case xevent.CreateNotifyEvent:
				createEvent := event.(xevent.CreateNotifyEvent)
				window := createEvent.Window

				windowProperties, err := xproto.GetProperty(X, false, window, xproto.AtomWmName, xproto.GetPropertyTypeAny, 0, (1<<32)-1).Reply()
				if err != nil {
					log.Println("Unable to get window properties")
					continue
				}

				windowName := strings.Split(string(windowProperties.Value), "-")[1]
				newWindow := Window{int64(window), windowName}

				if re.MatchString(windowName) {
					matchingWindows = append(matchingWindows, newWindow)
				}

			case xproto.DestroyNotifyEvent:
				destroyEvent := event.(xproto.DestroyNotifyEvent)
				window := destroyEvent.Window


				for i, w := range matchingWindows {
					if w.ID == int64(window) {
						matchingWindows = append(matchingWindows[:i], matchingWindows[i+1:]...)
						break
					}
				}
			}
		}
	}()

}
