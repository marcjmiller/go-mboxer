package gomboxer

import (
	"context"
	"log"

	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/keybind"
	"github.com/BurntSushi/xgbutil/xevent"
)

var (
	KB     *xgbutil.XUtil
	Leader int
)

func NewHotkeys() *Hotkeys {
	return &Hotkeys{}
}

func (hk *Hotkeys) Startup(ctx context.Context) {
	hk.ctx = ctx

	KB, err = xgbutil.NewConn()
	if err != nil {
		log.Printf("Error connecting to X: %v", err)
	}

	keybind.Initialize(KB)

	cb1 := keybind.KeyPressFun(func(KB *xgbutil.XUtil, e xevent.KeyPressEvent) {
		hk.ActivatePreviousWindow()
	})

	cb2 := keybind.KeyPressFun(func(KB *xgbutil.XUtil, e xevent.KeyPressEvent) {
		hk.ActivateNextWindow()
	})

	cb3 := keybind.KeyPressFun(func(KB *xgbutil.XUtil, e xevent.KeyPressEvent) {
		hk.ActivateLeaderWindow()
	})

	err = cb1.Connect(KB, KB.RootWin(), "Mod4-j", true)
	err = cb2.Connect(KB, KB.RootWin(), "Mod4-k", true)
	err = cb3.Connect(KB, KB.RootWin(), "Mod4-l", true)
	log.Println("Registered hotkeys")

	log.Println("Listening for HotKeys")
	xevent.Main(KB)
}

func (hk *Hotkeys) beforeClose(ctx context.Context) (prevent bool) {
	keybind.Detach(KB, KB.RootWin())
	return false
}

func (hk *Hotkeys) shutdown(ctx context.Context) {
	keybind.Detach(KB, KB.RootWin())
}

func (hk *Hotkeys) ActivateLeaderWindow() {
	if len(Windows) > 0 {
		log.Printf("Activating leader: %v \n", Windows[Leader].Name)
		LastFocused = 0
		NewApp().FocusWindow(Windows[Leader].ID)
	} else {
		log.Print("No windows to switch to")
	}
}

func (hk *Hotkeys) ActivateNextWindow() {
	var NextWindow = 0
	if len(Windows) == 0 || len(Windows) == 1 {
		log.Print("No windows to switch to")
	} else {
		NextWindow = LastFocused + 1
		if NextWindow >= len(Windows) {
			NextWindow = 0
		}
		log.Printf("Activating next window, %v \n", Windows[NextWindow].Name)
		LastFocused = NextWindow
		NewApp().FocusWindow(Windows[NextWindow].ID)
	}
}

func (hk *Hotkeys) ActivatePreviousWindow() {
	var PreviousWindow = 0
	if len(Windows) == 0 || len(Windows) == 1 {
		log.Print("No windows to switch to")
	} else {
		PreviousWindow = LastFocused - 1
		if PreviousWindow < 0 {
			PreviousWindow = len(Windows) - 1
		}
		log.Printf("Activating previous window, %v \n", Windows[PreviousWindow].Name)
		LastFocused = PreviousWindow
		NewApp().FocusWindow(Windows[PreviousWindow].ID)
	}
}
