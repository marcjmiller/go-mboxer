## About

`go-mboxer` is something I wrote to make multiboxing (EVE Online) better for linux users. Currently, there's [EVE-O Preview](https://github.com/EveOPlus/eve-o-preview), but it isn't linux-friendly, so `go-mboxer` was born. I hope to get to a point where it has feature parity with EVE-O Preview, but we'll see how far I get. Here's the list of features I imagine, feel free to add feature request issue if there's something you'd like to add. Contributions are welcomed, `go-mboxer` will always be free but if you wish to donate ISK to support me, send to `WTS JooKrap` 

## Features  
- [x] List EVE Online windows  
  - [ ] Work with other games, too   
    - [ ] User-Configurable games + custom game support
    - [ ] In-App shortcut to submit custom game support requests
- [x] Highlight currently focused window  
- [x] Allow focusing a window 
  - [x] Utilize global - hotkeys to swap windows/focus a specific 
  window  
    - [ ] Hotkeys should be user-configurable  
- [ ] Show window previews 
- [ ] Light/Dark theme  
  - [ ] Possible theme selection menu  

## Tech Used
UI:  
- [Wails](https://www.wails.io)  
- [React](https://reactjs.org/)  
- [Typescript](https://www.typescriptlang.org/)  
- [Tailwind-CSS](https://tailwindcss.com/)  

Backend:  
- [Golang](https://go.dev/)
