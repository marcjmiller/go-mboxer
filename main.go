package main

import (
	"embed"

	"go-mboxer/settings"
	gomboxer "go-mboxer/go-mboxer"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
)

//go:embed all:frontend/dist
var assets embed.FS

//go:embed build/appicon.png
var icon []byte

func main() {
	app := gomboxer.NewApp()
	hotkeys := gomboxer.NewHotkeys()
	config := settings.NewConfiguration()


	assetServerOpts := &assetserver.Options{
		Assets:  assets,
		Handler: nil,
	}

	wails.Run(&options.App{
		Title:       "go-mboxer",
		Width:       640,
		MinWidth:    640,
		Height:      480,
		MinHeight:   480,
		AssetServer: assetServerOpts,
		OnStartup:   app.Startup,
		Bind: []interface{}{
			app,
			hotkeys,
			config,
		},
	})
}
