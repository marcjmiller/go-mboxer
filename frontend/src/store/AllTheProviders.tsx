import { ReactNode } from 'react'
import ClientProvider from './ClientContext'
import SettingsProvider from './SettingsContext'
import LogProvider from './LogContext'
import { ViewProvider } from './ViewContext'

const AllTheProviders = ({ children }: { children: ReactNode | ReactNode[] }) => {
  return (
    <SettingsProvider>
      <ViewProvider>
        <ClientProvider>
          {children}
        </ClientProvider>
      </ViewProvider>
    </SettingsProvider>
  )
}

export default AllTheProviders
