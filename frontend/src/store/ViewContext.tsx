import { createContext, Reducer, useReducer } from 'react'

export enum Views {
  Active_Clients = 'Active Clients',
  Settings = 'Settings',
  About = 'About',
}

export function createCtx<StateType, ActionType>(
  reducer: Reducer<StateType, ActionType>,
  initialState: StateType
) {
  const defaultDispatch: React.Dispatch<ActionType> = () => initialState
  const ctx = createContext({
    state: initialState,
    dispatch: defaultDispatch,
  })
  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, dispatch] = useReducer<React.Reducer<StateType, ActionType>>(
      reducer,
      initialState
    )
    return <ctx.Provider value={{ state, dispatch }} {...props} />
  }
  return [ctx, Provider] as const
}

const initialState = { view: Views.Active_Clients }

type AppState = typeof initialState
type Action = { type: 'reset' } | { type: 'update'; payload: Views }

function reducer(state: AppState, action: Action): AppState {
  switch (action.type) {
    case 'reset':
      return { view: Views.Active_Clients }
    case 'update':
      return { view: action.payload }
    default:
      throw new Error()
  }
}

const [ctx, provider] = createCtx(reducer, initialState)

export const ViewContext = ctx
export const ViewProvider = provider
