import { createContext, ReactNode, useEffect, useState } from 'react'
import { GetClients, GetFocusedWindow } from '../../wailsjs/go/gomboxer/App'

interface Window {
  ID: number
  Name: string
}

interface IClientContext {
  clients: Window[] | null
  setClients: React.Dispatch<React.SetStateAction<Window[]>>
  activeClient: number
  setActiveClient: React.Dispatch<React.SetStateAction<number>>
}

export const ClientContext = createContext<IClientContext>({} as IClientContext)

const ClientProvider = ({ children }: { children: ReactNode | ReactNode[] }) => {
  const [clients, setClients] = useState<Window[]>([])
  const [activeClient, setActiveClient] = useState(0)

  useEffect(() => {
    const getClientsInterval = setInterval(() => GetClients().then((win) => setClients(win)), 1500)
    return () => clearInterval(getClientsInterval)
  }, [])

  useEffect(() => {
    const getActiveClientInterval = setInterval(
      () => GetFocusedWindow().then((focused) => setActiveClient(focused)),
      2000
    )
    return () => clearInterval(getActiveClientInterval)
  }, [])

  const props = {
    clients,
    setClients,
    activeClient,
    setActiveClient,
  }

  return <ClientContext.Provider value={props}>{children}</ClientContext.Provider>
}

export default ClientProvider
