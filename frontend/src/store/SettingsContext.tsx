import { createContext, ReactNode, useEffect, useState } from 'react'
import { GetSettings, GetGames } from '../../wailsjs/go/settings/Configuration'

export const SettingsContext = createContext<ISettingsContext>({} as ISettingsContext)

interface Settings {
  [key: string]: string
}

interface ISettingsContext {
  settings: Settings
  games: string[]
}

const SettingsProvider = ({ children }: { children: ReactNode | ReactNode[] }) => {
  const [settings, setSettings] = useState<Settings>({})
  const [games, setGames] = useState<string[]>([])

  useEffect(() => {
     GetSettings().then((cfg) => setSettings(cfg))
     GetGames().then((g) => setGames(g))
  }, [])

  const output = {settings, games}
  return <SettingsContext.Provider value={output}>{children}</SettingsContext.Provider>
}

export default SettingsProvider
