import { useContext } from 'react'
import { clsx } from 'clsx'
import { ViewContext, Views } from '../store/ViewContext'

const Nav = () => {
  const { state, dispatch } = useContext(ViewContext)

  function enumKeys<O extends object, K extends keyof O = keyof O>(obj: O): K[] {
    return Object.keys(obj).filter((k) => Number.isNaN(+k)) as K[]
  }

  return (
    <div className='nav'>
      <ul>
        {enumKeys(Views).map((value) => {
          if (value === Views.About) {
            return
          }
          return (
            <li
              key={value}
              className={clsx('button', state.view == Views[value] && 'active')}
              onClick={() => dispatch({ type: 'update', payload: Views[value] })}
            >
              {Views[value]}
            </li>
          )
        })}
      </ul>
      <div
        className={clsx('justify-self-end button', state.view == Views.About && 'active')}
        onClick={() => dispatch({ type: 'update', payload: Views.About })}
      >
        About
      </div>
    </div>
  )
}

export default Nav
