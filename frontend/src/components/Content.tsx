import { useContext } from 'react'
import { ViewContext, Views } from '../store/ViewContext'
import About from '../views/About'
import Clients from '../views/Clients'
import Settings from '../views/Settings'

const Content = () => {
  const { state } = useContext(ViewContext)

  switch (state.view) {
    case Views.Active_Clients:
      return <Clients />

    case Views.Settings:
      return <Settings />

    case Views.About:
      return <About />

    default:
      return <div>Oof, something went wrong.</div>
  }
}

export default Content
