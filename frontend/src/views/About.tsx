import { BrowserOpenURL } from '../../wailsjs/runtime'

const About = () => {
  return (
    <div className='content'>
      <div>
        <div className='text-xl font-bold'>About</div>
        <div className='mt-4'>
          go-mboxer is an open-source application intended to improve the ability of players who
          wish to multi-box games on Linux. I have no intention of ever asking for any fees to use
          go-mboxer, but donations are accepted.
        </div>
      </div>

      <div
        className='text-blue-500 hover:cursor-pointer self-center'
        onClick={() => BrowserOpenURL('https://gitlab.com/marcjmiller/go-mboxer')}
      >
        View the source
      </div>
    </div>
  )
}

export default About
