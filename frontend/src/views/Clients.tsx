import clsx from 'clsx'
import { useContext, useState } from 'react'
import { ElectNewLeader } from '../../wailsjs/go/gomboxer/App'
import { ClientContext } from '../store/ClientContext'

const Clients = () => {
  const { clients, activeClient } = useContext(ClientContext)
  const [leader, setLeader] = useState(0)

  const electNewLeader = async (newLeader: number) => {
    if (leader !== newLeader) {
      await ElectNewLeader(newLeader).then((res) => res && setLeader(newLeader))
    }
  }

  return (
    <div className='content'>
      <div className='text-xl font-bold'>Clients</div>
      <table className='table-auto w-full'>
        <thead>
          <tr>
            <th>ID</th>
            <th>Character</th>
            <th>Leader</th>
          </tr>
        </thead>
        <tbody className='text-center'>
          {clients ? (
            clients.map((client, idx) => (
              <tr
                key={client.ID}
                className={clsx(client.ID === activeClient && 'font-bold text-green-400')}
              >
                <td>{client.ID}</td>
                <td>{client.Name}</td>
                <td>
                  <input
                    type='radio'
                    onClick={() => electNewLeader(idx)}
                    name='leader'
                    checked={idx === leader}
                  />
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={3}>No EVE windows found!</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

export default Clients
