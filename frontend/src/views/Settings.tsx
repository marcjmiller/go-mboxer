import { useContext } from 'react'
import { SettingsContext } from '../store/SettingsContext'

const Settings = () => {
  const { settings, games } = useContext(SettingsContext)

  return (
    <div className='content'>
      <div className='text-xl font-bold'>Settings</div>
      <table className='table-auto mx-2 w-5/6 max-w-sm text-center'>
        <tbody>
          {Object.keys(settings).map((key, i) => (
            <tr key={i}>
              <td className='text-right'>{key}:</td>
              <td className='setting'>
                <SettingsInput setting={key} value={settings[key].toString()} games={games} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

const SettingsInput = ({
  setting,
  value,
  games,
}: {
  setting: string
  value: string
  games: string[]
}) => {
  switch (setting.toLowerCase()) {
    case 'game':
      return (
        <select className='setting' value={value} disabled>
          {games.map((game) => (
            <option value={game} selected={game === 'EVE'}>
              {game}
            </option>
          ))}
        </select>
      )

    default:
      return <input className='setting' value={value} disabled />
  }
}

export default Settings
