// import wailsLogo from "./assets/wails.png";
import './App.css';
import Content from './components/Content';
import Nav from './components/Nav';

const App = () => {
  return (
    <div className='min-h-screen text-white flex flex-row font-mono'>
      <Nav />
      <Content />
    </div>
  );
};

export default App;
