package settings

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Configuration struct {
	LogFilePath   string `yaml:"logfile_path"`
	Theme         string `yaml:"theme"`
	ShowDebugInfo bool   `yaml:"show_debug"`
	LogLevel      string `yaml:"log_level"`
	Game          string `yaml:"game"`
}

var (
	Config     Configuration
	homeDir    string
	configPath string
)

var SupportedGames =  []string{"EVE"}

func NewConfiguration() *Configuration {
	Read()
	return &Configuration{}
}

func Read() (*Configuration, error) {
	configDir, err := getConfigDir()
	if err != nil {
		return nil, fmt.Errorf("Error getting config dir: %w", err)
	}

	err = os.MkdirAll(configDir, 0755)
	if err != nil {
		return nil, fmt.Errorf("Error creating config dir: %w", err)
	}

	configFile := fmt.Sprintf("%v/config.yaml", configDir)
	file, err := os.OpenFile(configFile, os.O_RDONLY|os.O_CREATE, 0644)
	if err != nil {
		return nil, fmt.Errorf("Error opening config file: %w", err)
	}

	data, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("Error reading config file: %w", err)
	}

	isConfigEmpty := len(data) == 0

	if len(data) == 0 {
		log.Println("Config file not found, writing a default")
		data = []byte("logfile_path: /tmp/go-mboxer.log\ntheme: dark\nshow_debug: true\nlog_level: INFO\ngame: EVE\n")
	}

	err = yaml.Unmarshal(data, &Config)
	if err != nil {
		return nil, fmt.Errorf("Error reading settings file: %w", err)
	}

	if isConfigEmpty {
		log.Println("Writing settings to file")
		Write(&Config)
	}

	log.Println("Settings loaded:")
	fmt.Printf("  LogFile Path: %s\n", Config.LogFilePath)
	fmt.Printf("  Theme: %s\n", Config.Theme)
	fmt.Printf("  Show Debug Info: %v\n", Config.ShowDebugInfo)
	fmt.Printf("  Log Level: %s\n", Config.LogLevel)

	return &Config, nil
}

func Write(config *Configuration) error {
	data, err := yaml.Marshal(config)
	if err != nil {
		return fmt.Errorf("error marshalling config: %w", err)
	}

	configPath, err := getConfigDir()
	if err != nil {
		return fmt.Errorf("error getting config path: %w", err)
	}
	configFile := fmt.Sprintf("%v/settings.yaml", configPath)

	_, err = os.OpenFile(configFile, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("error opening settings file for writing: %w", err)
	}

	err = ioutil.WriteFile(configFile, data, 0644)
	if err != nil {
		return fmt.Errorf("error writing file: %w", err)
	}

	return nil
}

func getConfigDir() (string, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", fmt.Errorf("Error getting home dir: %w", err)
	}

	configDir := fmt.Sprintf("%v/.config/go-mboxer/", homeDir)

	return configDir, nil
}

func (c *Configuration) GetSettings() Configuration {
	return Config
}

func (c *Configuration) GetGames() []string {
	return SupportedGames
}